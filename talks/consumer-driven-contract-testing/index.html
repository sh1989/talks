<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Consumer-Driven Contract Testing</title>
    <link rel="stylesheet" href="../lib/reset.css">
    <link rel="stylesheet" href="../lib/reveal.css">
    <link rel="stylesheet" href="../lib/theme/hogy-white.css">
		<link rel="stylesheet" href="../lib/plugin/highlight/monokai.css">
  </head>
  <body>
    <div class="reveal">
      <div class="slides">
        <section id="title">
          <h2 class="r-fit-text">Consumer-Driven Contract Testing</h2>
          <p><a href="https://samhogy.co.uk">Sam Hogarth</a></p>
        </section>
        <section id="deming">
          <blockquote>“Quality cannot be inspected into a product or service; it must be built into it.”<br />
            <a href="https://deming.org/inspection-is-too-late-the-quality-good-or-bad-is-already-in-the-product/">W Edwards Deming</a>
          </blockquote>
          <aside class="notes">
            This is from a manufacturing-line context, but applied to software...
            Inspection can identify unsatisfactory software which must be fixed via rework. Cost of delay.
            It doesn't improve the process, so it cannot improve quality long-term.
            And it becomes a barrier to delivery over time which slows everything down.
          </aside>
        </section>
        <section id="continuous-delivery">
          <h2 class="r-fit-text">CD Requires Fast Feedback</h2>
          <ul>
            <li>✔️ Independent deployability</li>
            <li>✔️ Automation</li>
            <li>✔️ Shifting feedback left</li>
            <li>✔️ Collaboration over handoffs</li>
          </ul>
          <aside class="notes">
            To support the continuous delivery of value to our customer we need to build in quality to our systems.
            Rather than big bangs of a large number of software components.
            Reducing human toil and improving Reliability.
            Gaining faster feedback about defects to minimise the cost of rework.
            Working multi-disciplinary rather than in functional silos.
          </aside>
        </section>
        <section id="e2e">
          <h2>End to End</h2>
          <section>
            <p>Challenges:
              <ul>
                <li>Speed of feedback</li>
                <li>Reliability of feedback</li>
                <li>Managing unowned dependencies</li>
              </ul>
            </p>
            <aside class="notes">
              This is conventionally where a lot of inspection happens, in its integration
            </aside>
          </section>
          <section>
            <blockquote>"Any advantage you gain by talking to the real system is overwhelmed by the need to stamp out non-determinism"<br />
              <a href="https://www.martinfowler.com/articles/nonDeterminism.html">Martin Fowler</a>
            </blockquote>
          </section>
          <section>
            <blockquote>"Push tests as low as they can go for the highest return in investment and quickest feedback"<br />
            <a href="https://agiletester.ca/more-agile-testing-the-book/">Janet Gregory and Lisa Crispin</a></blockquote>
            <aside class="notes">
              This is how we stamp out the non-determinism, by finding a more deterministic failure signal.
              And that can often be done by testing lower down in the testing pyramid.
            </aside>
          </section>
          <section>
            <p>Decomposing what we do:</p>
            <ol>
              <li>Connectivity <em class="fragment light">- Mocks and Smoke Testing</em></li>
              <li>Conversation <em class="fragment">- Contract Testing</em></li>
              <li>Conduct <em class="fragment">- Contracts, Monitoring, User Journeys</em></li>
            </ol>
          </section>
        </section>
        <section id="contract-testing-intro">
          <h2 class="r-fit-text">Contract Testing</h2>
          <p>Capture service communication in a contract</p>
          <img src="./assets/saka.jpg" class="r-stretch" />
          <aside class="notes">
            Capture the communication in a form that makes it easy to test on both sides, independently.
            You're not "testing" the functionality of the service here, you're describing how it's used (Interactions, usage and expectations).
            If there are any expectations on data, such as the format of a date, then it's a good place to make those assertions.
          </aside>
        </section>
        <section id="pact">
          <img src="./assets/pact-logo.png" />
          <p><a href="https://pact.io/">Pact</a> terminology:</p>
          <ul>
            <li>Consumer of a service</li>
            <li>Provider</li>
            <li>Pactfiles</li>
            <li>Broker</li>
          </ul>
        </section>
        <section id="consumer">
          <h2>Consumer</h2>
          <ol>
            <li class="fragment">🧪 Write unit tests demonstrating usage</li>
            <li class="fragment">📜 Run tests against a mock to generate contracts</li>
            <li class="fragment">💾 Upload to Broker during CI</li>
          </ol>
          <aside class="notes">
            Consumer-side unit testing of service calls against mocked responses.
            The Pact mock will automatically record and generate Pactfiles.
            You can use your own mocking tool such as Wiremock, Cypress, Playwright and there are open source adapters
            to generate Pactfiles from this.
            Consumer driven, not dictated. These should capture conversations.
          </aside>
        </section>
        <section>
          <p>A Pactfile viewed in the Pact Broker UI</p>
          <img src="./assets/pact-contract.png" class="r-stretch" />
        </section>
        <section id="provider">
          <h2>Provider</h2>
          <section>
            <p>Does this build satisfy the consumer's contracts?</p>
          </section>
          <section>
            <p>During CI...</p>
            <ol>
              <li class="fragment">📜 Retrieve mainline contract for each consumer</li>
              <li class="fragment">🧪 Start service (mock upstream dependencies)</li>
              <li class="fragment">🚀 Launch Pact mock client</li>
              <li class="fragment">✔️ Verify service's responses against contract</li>
              <li class="fragment">💾 Upload verification results to Broker</li>
            </ol>
          </section>
          <section>
            <p>When a new consumer contract is published...</p>
            <ol>
              <li class="fragment">⚡ Special CI provider job triggered by Broker</li>
              <li class="fragment">✔️ Runs provider verification against new contract</li>
              <li class="fragment">💾 Upload verification result to Broker</li>
            </ol>
          </section>
        </section>
        <section>
          <img src="./assets/pact-broker.png" />
        </section>
        <section>
          <img src="./assets/pact-broker-matrix.png" />
          <aside class="notes">
            Pact transparently handles Pactfiles not changing between commits.
            It builds up a compatibility matrix of which builds work with each other.
          </aside>
        </section>
        <section id="broker-network-graph">
          <img src="./assets/gds-pact-broker-network.png" />
          <p>From the <a href="https://pact-broker.cloudapps.digital/">UK Govt’s (Public) Pact Broker Instance</a></p>
        </section>
        <section id="cd">
          <h2>Time to Deploy!</h2>
          <section>
            <img src="./assets/pact-broker-environments.png" />
          </section>
          <section>
            <p>Pre-</p>
            <pre>
              <code data-trim class="bash">
pact-broker can-i-deploy
  --pacticipant=pact-consumer
  --version=$CI_COMMIT_SHORT_SHA
  --to-environment=production
              </code>
            </pre>
          </section>
          <section>
            <p>Post-</p>
            <pre>
              <code data-trim class="bash">
pact-broker record-deployment
  --pacticipant=pact-consumer
  --version=$CI_COMMIT_SHORT_SHA
  --environment=production
              </code>
            </pre>
          </section>
        </section>
        <section>
          <section>
            <p>Deploy consumer (when provider isn't deployed)</p>
            <img src="./assets/deploy-when-provider-not-in-environment.png" />
          </section>
          <section>
            <p>Deploy provider</p>
            <img src="./assets/deploy-pact-provider.png" />
          </section>
          <section>
            <p>Deploy consumer</p>
            <img src="./assets/deploy-pact-consumer.png" />
          </section>
          <section>
            <p>Deploy consumer (with unsupported changes)</p>
            <img src="./assets/deploy-incompatible-consumer.png" />
          </section>
        </section>
        <section id="theres-more">
          <img src="./assets/but-wait-theres-more.gif" />
          <ul>
            <li>Provider States</li>
            <li>Pending Pacts</li>
            <li>WIP Pacts</li>
            <li>Bi-directional Contract Testing</li>
          </ul>
        </section>
        <section id="homework">
          <h2>Further Reading</h2>
          <ul>
            <li><a href="https://gitlab.com/my-pact-experiments">Sample Code</a></li>
            <li><a href="https://samhogy.co.uk/2021/03/consumer-driven-contracts-with-pact/">Associated Blog Series</a></li>
          </ul>
        </section>
      </div>
    </div>
    <script src="../lib/reveal.js"></script>
    <script src="../lib/plugin/highlight/highlight.js"></script>
    <script src="../lib/plugin/notes/notes.js"></script>
    <script>Reveal.initialize({ hash: true, plugins: [RevealHighlight, RevealNotes] });</script>
  </body>
</html>