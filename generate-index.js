const fs = require('fs');
const path = require('path');
const pug = require('pug');

const encoding = 'utf8';
const title = `Sam's Tech Talks`;
const outputDir = '.public';

const talks = fs.readdirSync('talks', { encoding })
  .filter(x => x !== 'lib');

const html = pug.renderFile('_template/index.pug', {
  title,
  talks,
  pretty: true
});

if (!fs.existsSync(outputDir)) {
  fs.mkdirSync(outputDir);
}

fs.writeFileSync(path.join(outputDir,  'index.html'), html, { encoding });