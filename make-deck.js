const fs = require('fs');
const path = require('path');
const pug = require('pug');

const encoding = 'utf8';

const title = 'Consumer-Driven Contract Testing';
const outputDir = 'talks/consumer-driven-contract-testing';

const html = pug.renderFile('_template/slideshow.pug', {
  title,
  pretty: true
});

if (!fs.existsSync(outputDir)) {
  fs.mkdirSync(outputDir);
}


fs.writeFileSync(path.join(outputDir,  'index.html'), html, { encoding });